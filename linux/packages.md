apt install
    - Editores
        - nano
        - vim

    - Desenvolvimento/Servidor
        - apache2
            - php
            - mysql
            - bind9
            - pm2
            - ngrok
            - git
            - nodejs
            - python
        - nginx
        - samba
        - ssh
        - tree

    - Monitoramento
        - zabbix
        - htop
        - neofetch
        - fdisk

    - Gerenciador de pacotes
        - aptitude
        - yum
        - apt-get
        - zipper
        - urpmi
        - imerge
        - pacman
    
    - Gerenciador de Redes
        - net-tools

- man