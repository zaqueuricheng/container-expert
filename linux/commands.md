systemctl <option> <service>
    <options>
        - status
        - restart
        - start
        - stop
        - poweroff
        - reboot
        - reload
        - suspend
        - hibernate
        - enable

    <services>
        - networking
        - ssh
        - mysql
        - apache2
        - zabbix-agent
        - zabbix-server

whoami
adduser
su <your-user>
echo
cd 
touch
chmod +X <file>
mv <file> <path>
cat <file>
date
df -h
df -i
lsblk
du -h
history(1000)
netstat
