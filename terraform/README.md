https://www.youtube.com/watch?v=UCn3kTEMfCM

IaC - Infraestruture as Code

Terraform/Azure
Terraform é uma ferramenta openSource que permite construir e versionar a infraestrura de forma segura.

Basics Concepts
    - Terraform files
    - Providers -  São as conexões com os provedores
    - Resource - 
    - Initialization
    - Apply - constroi a infra
    - State
    - Variables
    - Provisioner
    - Destroy - destoi toda a infra

Install
    - choco install terraform

Deploy

Auto stop/start Cluster AKS
    - https://stackoverflow.com/questions/48367923/stop-all-compute-in-aks-azure-managed-kubernetes#:~:text=The%20AKS%20Stop%2FStart%20cluster,button%2C%20saving%20time%20and%20cost.