# Day-01 Helm
## Basics
- HELM- é como se fosse um apt-get, ele facilita a instalação DEPLOYMENT de aplicações dentro do K8S.
- DEPLOYMENT - arquivo yaml
- SERVICE - Maneira de expor a sua aplicação - NodePort | ClusterIP | LoadBalancer
- INGRESS - Maneira das pessoas acessarem seus serviços, pode definir URL, ROTAS...

- criação de CHARTS - Um chart é como se fosse um "PACKAGE" onde tem todas as instruções (SERVICE, INGRESS...) para DEPLOYAR sua aplicação.
- criação de TEMPLATES

## Instalation
- https://helm.sh/
- https://artifacthub.io/
- https://kind.sigs.k8s.io/

## Create AKS cluster to deploy helm charts
```
- az login
- az group create --name LIVE --location eastus
- az aks create --resource-group LIVE --name LIVE-AKS --node-count 2 --enable-addons monitoring --generate-ssh-keys
- az aks get-credentials --resource-group LIVE --name LIVE-AKS
```

## add REPOSITORIO - Local na internet onde fica armazenado os CHARTS, geralmente fica no artifact HUB
```
- helm repo add stable https://charts.helm.sh/stable
- helm search repo stable
- helm search hub
- helm search hub nginx
- helm repo update
```

## Install CHARTS (Eh como se fosse um apt-get)
```
- helm install stable/mysql --generate-name ## Faz a instalação sem customizar o chart
- kubectl get pods 
- kubectl get pods --all-nmaespaces
- kubectl get svc
- helm list ## Mostra os charts q estão instalados no k8s
- helm status $CHART_NAME ## Mostra as informações de conexão do chart
- helm uninstall $CHART_NAME
- kubectl logs $CHART_NAME -n data-bases
- helm show chart stable/mysql
```

## Charts/packages
- mkdir /helm/giropops
- cd /helm/giropops
- helm create giropops
- apt install tree

## 
    - deployment.yaml -> Questionario
    - values.yaml -> Respostas

##
    - helm install giropops --values values.yaml
    - helm install giropops giropops/ --values giropops/values.yaml
    - helm upgrade giropops giropops/ --values giropops/values.yaml
    - helm ls
    - kubectl get pods
    - helm rollback giropops 1
    - kubectl get pods giropops-854f6bf45c-5hlcc -o yaml
    - kubectl top nodes
    - kubectl top pods
    - helm history giropops

## Day 2