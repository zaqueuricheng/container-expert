# Virtual Machines (Bridge internet mode on virtualbox)
- 1 - node-1 -> 192.168.0.65
- 2 - node-2 -> 192.168.0.69
- 3 - node-3 -> 192.168.0.72
- 4 - zabbix/mysql -> 192.168.0.70
- 5 - pfsense
- 6 - hitss -> 192.168.0.67
- 7 - raspberry pi -> 
- 8 - windows 10 -> 

# Local Machine IP
- ipconfig
- 192.168.0.100
- timedatectl set-timezone America/Sao_Paulo

# Connect and test connect
- ping 192.168.0.70 ## Teste de envio de pacotes
- telnet 192.168.0.70 22 ## Testa se a porta 22 do host está funcionando
- ssh zabbix@192.168.0.70
- ssh node-1@192.168.0.65
- ssh node-2@192.168.0.69
- ssh node-3@192.168.0.72
- 192.168.0.67 ## hitss-linux-machine

# Linux Commands
- hostnamectl # Mostra as configurações básica do host
- uptime
- top | grep up
- ip address
- ip a
- ip addr
- ping
- traceroute
- telnet
- netstat -pnltu
- systemd-analyze
- systemd-analyze blame
- poweroff
- shutdown -h now
- halt -p
- systemctl is-enabled ssh
- systemctl start/stop/restart/enable/disable/halt/poweroff/reboot/hibernate
- systemctl --host=node-1@192.168.0.65
- systemctl --help
- fdisk -l
- df -h
- lsblk
- lsblk -f
- tail
- sudo tail -n 2 /var/log/apache2/error.log ## Mostra as 2 últimas linhas dos logs
- sudo tail -n 5 /var/log/apache2/error.log | grep line
- less
- file -sL /dev/sd*
- resize2fs /dev/sdb
- hostname

# Diretorios linux
- /etc/network
- /etc/hosts
- /etc/exports
- /dev

# Pacotes linux
- apt install traceroute
- apt install net-tools
# Servidores 
## 1 - Monitoração 
## 1.1 - Zabbix server e zabbix agent - https://linoxide.com/how-to-install-zabbix-server-on-ubuntu-20-04/
## Install apache2
- apache2 ## Estudar mais este servidor
- apache webserver -> 192.168.0.70:80
- logs -> /var/log/apache2/error.log

## Install and configure PHP
- php ## Estudar 
- php -> /etc/php/7.4/apache2/php.ini
max_execution_time 300
max_input_time 300
memory_limit 256M
post_max_size 16M
upload_max_filesize 16M
date.timezone = 'America/Sao_Paulo'
max_input_vars 10000

## Install mariadb
- Criate user and database
sudo mysql -u root -p
CREATE DATABASE zabbix_db character set utf8 collate utf8_bin;
CREATE USER 'zabbix_user'@'localhost' IDENTIFIED BY '92Z@queu';
GRANT ALL PRIVILEGES ON zabbix_db.* TO 'zabbix_user'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;

## Install zabbix server
- nano /etc/zabbix/zabbix_server.conf
DBHost=localhost
DBName=zabbix_db
DBUser=zabbix_user
DBPassword=P@ssword321

Then load the default schema of the Zabbix database

http://192.168.0.70/zabbix

## Segue configurando no browser
Username: Admin
Password: zabbix

## Install agent on hosts - https://linoxide.com/install-zabbix-agent-on-ubuntu-20-04/
sudo systemctl is-enabled zabbix-agent
sudo systemctl enable zabbix-agent
sudo netstat -pnltu
sudo nano  /etc/zabbix/zabbix_agentd.conf

## Configure others thinks on Zabbix


## 2 - Banco de dados (mysql, postgres, rethink e mongo) - Estudar cada um deles
## 3 - Web (nginx, apache2 e glassfish)
## 4 - Segurança (ufw, pfsense)
## 5 - Compartilhamento (nfs e samba)
## 6 - Impressão (cups)

# WinSCP

# Orquestração de containers
## docker swarm
## kubernetes
# Musica
## kill hill - 3o Episódio
## mc k
## Kool Klever
## Keta Mayanda
## Kalibrados
## Sandocam produziu vários artistas...