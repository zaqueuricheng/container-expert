# Basics
https://www.datacamp.com/community/tutorials/10-command-line-utilities-postgresql?utm_source=adwords_ppc&utm_campaignid=1455363063&utm_adgroupid=65083631748&utm_device=c&utm_keyword=&utm_matchtype=b&utm_network=g&utm_adpostion=&utm_creative=332602034358&utm_targetid=dsa-429603003980&utm_loc_interest_ms=&utm_loc_physical_ms=1001754&gclid=Cj0KCQjwsLWDBhCmARIsAPSL3_0dVNNZr2F5JKASeBKavMUhEQ0BKCmhlnZOjdxNa8jZ9AjqhvzDojoaArpgEALw_wcB

# Port-foward
- kubectl port-forward postgresql-staging-postgresql-0 5432:5432 -n data-bases

# Install postgresql cli
- https://wiki.postgresql.org/wiki/PostgreSQL_Clients

- https://www.postgresql.org/download/
    - user: postgres
    - pwd: postgres
    - port: 5433

# Atualizar as variáveis no path depois de instalar o postgres

# Commands
    - psql --help
    - psql --username=postgres --port=5433 --dbname=postgres

# Container conect
    - database: neuron
    - pwd: nwt9265uencfj8nc4
    
    - psql --host=localhost --username=postgres --port=5433 --dbname=neuron --password=nwt9265uencfj8nc4