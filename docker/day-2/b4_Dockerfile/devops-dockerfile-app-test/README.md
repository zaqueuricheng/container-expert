docker build -t flask-app:1.0.0 . #Cria a imagem da aplicação
docker images #Lista todas as imagens incluindo a criada pelo comando acima
docker run -p 5000:5000 --name flask-app -d flask-app:1.0.0 #Cria o container da aplicação
docker container ps #Lista todos os containers
docker logs <ID> #Mostra os logs da aplicação
curl localhost:5000 #Verifica a aplicação funcionando na porta 5000