https://ubuntu.com/download/desktop#download

HOW TO MOUNT K8S CLUSTER
    1 - Install tree virtual machine on hyper-v with ubuntu
        Names and Passwords of the machines:
            node-01 | 123
            node-02 | 123
            node-03 | 123
    2 - sudo apt-get update && upgrade
    3 - sudo apt-get install ssh
    4 - ssh -p 22 user@address # Entrar na máquina pelo terminal (Conemu) para facilitar copy past of command
    5 - df -h
    6 - swapoff -a # poweroff swap memory
    7 - vim /etc/fstab # comment the swap line
    8 - Intall docker: curl -fsSL https://get.docker.com | bash # Adiciona o repositório do docker e faz a instalação
    9 - docker container
    10 - docker volume
    10 - docker --version