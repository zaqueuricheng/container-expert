- https://www.youtube.com/watch?v=xxjzwdtWozI

- https://github.com/fabioruicci/django-postgresql-docker/blob/main/auladocker/settings.py

- py -m venv venv // Cria o diretorio venv de ambiente virtual
- .\venv\Scripts\activate
- pip install django
- django-admin startproject auladocker .
- py .\manage.py runserver // Roda o projeto no localhost

## requirement
- psycopg2-binary // Adapdaor para acessar o banco de dados

## dockerfile

## docker-compose

## configure postgresql
- /django/project-name/settings.py

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "postgres",
        "USER": "postgres",
        "PASSWORD": "postgres",
        "HOST": "db",
        "PORT": 5432,
    }
}

## 
- docker-compose up
- docker-compose down
- docker-compose up --build
- docker-compose up -d
- docker-compose logs