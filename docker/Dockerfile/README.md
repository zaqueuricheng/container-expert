
## Check docker
- docker --version
- docker-compose --version //
## Create your image
- docker build . -t <YOUR_IMAGE_NAME>:version // O ponto é para ele procurar o arquivo Dockerfile
- docker images 

## Create your container app
- docker run -p 5000:5000 --name <APP_NAME> -d <YOUR_IMAGE_NAME>:version
- docker container ps
- docker logs <CONTAINER_ID>

## Check your application
- curl localhost:5000

## Log on to container
- docker container exec -i -t <CONTAINER_ID> bash
- docker container exec -i -t <CONTAINER_ID> /bin/bash