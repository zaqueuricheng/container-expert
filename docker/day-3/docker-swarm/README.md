# Gerenciamento de containers

# Dois principais orquestradores:
- Docker swarm - <É um orquestrador q já vem inbutido no docker.>
- Kubernetes

# Comunicação entre os nodes
# Balanceamento de carga entre os nodes

# Swarm caracteristicas (papeis) importantes
- manager - <Faz a administração (trabalho mais nobre) dos containers, pode também ser executado containers aqui.>
- workers - <Onde fica os containers em execução.>

# Manager 
- 50% 
- 3 ou mais nodes
- A eleição de manager é feita toda vez q cai um node.
- Quanto maior a quantidade de nodes maior é o tempo de eleição.

# Docker-Swarm
- docker swarm --help
- docker command --help

# Criação do Cluster para o swarm
- Criar três maquinas virtual linux - ubuntu
    - sudo hostname node-01 <Renomear as maquinas caso necessário>
    - sudo su <Ir para a raiz do sistema root>

- Instalar o docker em uma das máquinas
    - curl -fsSL https://get.docker.com | bash
    - sudo usermod -aG docker your-user <Para dar permissão ao seu usuário>
    - docker swarm init <Geralmente essa maquina entra como manager>

- Instalar o docker nas outras duas máquinas
    - Copiar o "docker swarm join" e executar nas duas maquinas (node-02 e node-03)
    - docker swarm join --token <YOUR-TOKEN>
    - docker node ls <Lista todos os nodes envolvidos no cluster, esse comando só funciona no "lider">
    - docker node promote node-02 <Promove o node-02 para manager, com isso ele pode usar o comando "docker node ls">
    - docker node promote node-03 
    - docker node demote node-03 <Despromove o node-03>
    - docker swarm leave -f <Para sair do cluster>

# client docker engine ?
# server docker engine ?
# docker daemon ?

# Solve docker swarm problem
- docker swarm init --force-new-cluster
- docker swarm leave -f <Para sair do cluster>
- docker swarm init <Iniciar o cluster>
- docker swarm join-token manager
- docker swarm join-token worker
- docker swarm join-token --rotate manager <Para mudar o token>

# Create replicas on the cluster
- docker node ls
- docker service create --name webserver --replicas 3 -p 8080:80 nginx <Cria três replicas de container e distribui no cluster>
- docker service ps webserver

- docker node update --availability pause node-01 <Pausar o node para n receber container>
- docker node inspect node-01
- docker service scale webserver=10 <Escala o serviço para ter 10 containers>
- docker node update --availability active node-01 <Ativa o node para receber replicas>
- docker node update --availability drain node-01 <Mata os containers do node e sobe nos outros, usado geralmente para fazer manutenção/atualização do servidor>

# Service: 
- <Uma forma de você ter resiliência nos containers, blindar portas, balanceamento de cargas. Fazer os containers conversarem com o mundo. É basicamente um VIP ou DNS que faz todo o balanceamento de carga das requisições q vem de fora ou dentro do seu cluster.>

- redis: Guarda chaves

- docker service --help
    - docker service create --name giropops --replicas 3 --publish 8080:80 nginx <Publish (-p) blindar uma porta>
    - docker service ls
    - docker service ps giropops <Mostra onde os containers estão rodando>
    - docker service inspect giropops
    - docker service rm giropops

- docker volume ls
    - docker volume create giropops
    - cd /var/lib/docker/volumes/giropops/_data
        - vim index.html
        - cat index.html
    - docker service create --name giropops --replicas 3 -p 8080:80 --mount type=volume,src=giropops,dst=/usr/share/nginx/html nginx
    - docker service ls
    - docker service ps giropops

# No servidor 1
- apt install nfs-server <Para compartilhamente de dados> 
- nano /etc/exports -> /var/lib/docker/volumes/giropops *(rw,sync,subtree_check)
- cat /etc/exports
- exportfs -ar
- apt install net-tools <To use ifconfig and others tools>
- ifconfig eth0

# No servidor 2
- apt install nfs-common
- showmount -e 172.17.153.91 <IP do servidor, para checar se esta tudo bem>
- mount -t nfs 172.17.153.91:/var/lib/docker/volumes/giropops /var/lib/docker/volumes/giropops
- cd /var/lib/docker/volumes/giropops <Se fazer um ls e dar permissão negada, vai no server e faz um chmod 777 dir/ -R e monta novamente>
        
- Voltar no node-01 (Server) 
    - docker service create --name giropopstwo --replicas 3 -p 8081:80 --mount type=volume,src=giropops,dst=/usr/share/nginx/html nginx

# Service e network
- docker service create --name giropopstwo --replicas 3 -p 8081:80 --mount type=volume,src=giropops,dst=/usr/share/nginx/html --hostname test --limit-cpu 0.25 --limit-memory 64M --env chave=valor nginx

- docker service create --name nginex2 -p 8088:80 nginx
- docker service ls
- docker ps
- docker network ls <Lista as redes disponiveis, o ingress também é considerada como um "rede">
- docker service rm ID1 ID2 ID3

- docker network create giropops
- docker network ls
- docker network rm ID
- docker network create -d overlay giropops <Rede overlay é para swarm>
- docker service create --name nginx1 -p 8080:80 --network giropops nginx
- docker service ls
- docker service inspect nginx1

# Entra no container nginex1: docker container exec -ti 99aee085df4b bash
- apt update
- apt upgrade
- apt install curl
- curl nginx1
- curl nginx2
- echo "giropops" > /usr/share/nginx/html/index.html <Direciona o conteúdo no arquivo especificado>
- docker service scale nginx1=10
- docker service inspect nginx1 --pretty