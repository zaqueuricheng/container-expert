## Secrets

- Listar nodes
    - docker node ls
## Gerenciamento de secrets
- Criando um secret a partir da linha de comando
    - echo -n "GIROPOPS STRIGUS GIRUS" | docker secret create test - // add "string/key" no secret chamado test, -n para evitar quebra de linha
    - docker secret ls
    - docker secret inspect test

- Criando um secret a partir de um arquivo
    - vim test.txt // Criar o arquivo e add uma senha
    - docker secret create SECRET_NAME test.txt
    - docker secret ls

- Usar o secret
    - docker service create --name nginx -p 8080:80 --secret test-keys nginx
    - docker service rm SERVICE_ID
    - docker service scale nginx=3
    - docker container exec -ti CONTAINER_ID bash
    - cd /run/secrets # Local onde fica armazenado o secret dentro do container
    - cat SECRET_NAME
    - docker secret ls
    - docker secret inspect SECRET_ID
    - docker service update --secret-add SERVICE_NAME
    - docker service update --secret-rm SERVICE_NAME

    - NOTA: A maioria dos dados ficam ou são guardados no repo do git ou local, porém não é aconselhado guardar informações sencíveis como é o caso de secret. Normalmente o pessoal de DevOps trabalha com ferramentas de configurações como é o caso do Ansible, Chef ou Puppet.
    
    - docker service create --name nginx2 -p 8082:80 --secret src=SECRETE_NAME,target=meu-secret,uid=200,gid=200,mode=0000 nginx
    - docker container exec -ti CONTAINER_ID bash
    - cd /run/secrets
    - cat file
    - ls -lha # Para confirmar as permissões dadas quando se criou o service

    - NOTA: Não é possível utilizar secret fora do cluster "docker swarm"

# New Cluster
- 192.168.56.102 | node-1 | ssh node-1@192.168.56.102
- 192.168.56.103 | node-2 | ssh node-1@192.168.56.103
- 192.168.56.101 | node-3 | ssh node-1@192.168.56.101
