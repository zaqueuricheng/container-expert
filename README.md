# Container Expert

## GitLab
https://gitlab.com/zaqueuricheng/container-expert/-/tree/master
zaqueuricheng
.gitlab
## 1 - Docker
* Docker/Container
* Docker Machine
* Docker Swarm/Orquestrador
## 2 - Kubernetes
* Orquestrador
## 3 - Istio
## 4 - Linux
## 5 - Terraform/Ansible
* IaC - Infraestrutura como código
## 6 - Python/Golang/Shell Script/Power Shell Script
* Scripts
* Data Science

## VSCODE plug-ins
- vscode helm
- Azure Account
- Azure CLI Tools
- Azure Terraform
- Dart
- Docker
- Flutter
- Folder Templates
- Git History
- Git Lens
- HashiCorp Terraform
- Jupyter
- Kubernetes
- PHP Server
- Python
- Remote - WSL
- Terraform
- Terrafrom Autocomplete
- vscode-icons
- vscode-pdf
- YAML
